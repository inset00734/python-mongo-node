module.exports = function() {
	const express = require('express');
	const app = express();
	const open = require('open');
	const util = require('util');
	const fs = require('fs');
	const rootDir = require('app-root-dir').get();

	const PythonShell = require('python-shell');
	const pathToPy = `${rootDir}/py`;
	const options = {
		mode: 'text',
		scriptPath: pathToPy
	};
	const shell = new PythonShell('connect.py', options);
	
	const controllers = {
		getDataUsers: (req, res, next) => {
			let data = '';

			shell.on('message', (msg) => {
				message = msg.replace('\r','');

				switch (message) {
					case "done":
						fs.readFile(pathToPy + '/report.xlsx', (err, data) => {
							if (err) {
								console.log('ERR read data XLSX');
								throw err;
							} else {
								res.attachment('report.xlsx');
								return res.send(data);
							}							
						});
						break;
					case "error":
						console.log('ОШИБОЧКА ВЫШЛА =(');
						break;
					default:
						console.log('Default option.');
						break;
				}		
			})			
			
		}
	}

	app
	.get('/', [
		controllers.getDataUsers
	])
	.listen(8000, () => {
		console.log('Server started in 8000 port');
		open('http://localhost:8000', 'chrome');
	});

}
