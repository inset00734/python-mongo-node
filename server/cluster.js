const cluster = require('cluster'),
	  child_process = require('child_process');
	  //arguments = require('minimist')(process.argv.slice(2));

if (cluster.isMaster) {
	var cpuCount = require('os').cpus().length;

	cpuCount = 1;

	for (var i = 0; i < cpuCount; i += 1) {
		cluster.fork();
	}

	console.log('\x1b[32m');
	console.log('###############################################################');
	console.log('#\033[30C#\033[30C#');

	Object.keys(cluster.workers).forEach(function (id) {
		console.log('#  Worker is running with PID  #  ' + cluster.workers[id].process.pid, '\033[K', '\033[' + (25 - cluster.workers[id].process.pid.toString().length) + 'C', '#');
	});

	console.log('#\033[30C#\033[30C#');
	console.log('\x1b[32m%s\x1b[0m', '###############################################################');

	cluster.on('exit', function (worker, code, signal) {
		console.log('\x1b[31mWorker with PID' + worker.process.pid + ' died.');
		console.log('\x1b[33m%s\x1b[0m', 'Worker is restarted');
		cluster.fork();
	});
} else {
	require('./app.js')(cluster);
}