// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User	   		= require('../app/models/user');

// expose this function to our app using module.exports
module.exports = function(passport) {
	passport.createErorr = function(type, message) {
		var result = {
				error: {
					input: {}
				}
			};

		result.error.input[type] = message;

		return result;
	};

	// =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session

	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	passport.use('local-signup', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
		usernameField : 'key',
		passwordField : 'password',
		passReqToCallback : true // allows us to pass back the entire request to the callback
	},
	function(req, key, password, done) {
		var res = req.res,
			data = req.body;

		if (data.confirm_password !== password) {
			return res.status(403).json(passport.createErorr('confirm_password', 'Пароли не совпадают'));
		}
		
		if (!data.last_name) {
			return res.status(403).json(passport.createErorr('last_name', 'Укажите фамилию.'));
		}
		
		if (!data.first_name) {
			return res.status(403).json(passport.createErorr('first_name', 'Укажите имя.'));
		}

		User.findOne({ 
			invite_key: key
		}, function(error, user) {
			if (error || !user) {
				return res.sendStatus(404);
			}

			if (user.confirmed) {
				return res.sendStatus(405);
			}
			
			user.local.password = user.generateHash(password);
			user.confirmed = true;
			user.status = 'active';
			user.first_name = data.first_name;
			user.last_name = data.last_name;
			user.initials = user.last_name.charAt(0).toUpperCase() + user.first_name.charAt(0).toUpperCase();

			User.update(
				{
					invite_key: key,
					confirmed: false
				}, {
					$set: user
				}, function(error, response) {
					if (error) {
						return res.sendStatus(500);
					}

					return done(null, user);
				});
		});
	}));

	passport.use('local-login', new LocalStrategy({
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback : true
	},
	function(req, email, password, done, res) {
		var res = req.res;

		User
			.findOne({
				'local.email':  email
			},
			function(error, user) {
				if (error) {
					return res.sendStatus(400);
				}

				if (!user) {
					return res.status(403).json(passport.createErorr('email', 'Пользователя с данным email не существует'));
				}

				if (user.partner && typeof(user.partner) === 'string' && user.partner.length > 0) {
					return res.sendStatus(424);
				}
				
				if (user.status === 'invite') {
					return res.status(403).json(passport.createErorr('email', 'Пользователь с данным email не завершил регистрацию'));
				}
				
				if (user.status === 'inactive') {
					return res.status(403).json(passport.createErorr('email', 'Пользователю с данным email закрыт доступ'));
				}

				if (!user.validPassword(password)) {
					return res.status(403).json(passport.createErorr('password', 'Введен неверный пароль'));
				}

				return done(null, user);
			});
	}));
};
