import sys, os, json, pymongo, dateutil.parser
from bson import json_util, SON
from openpyxl import Workbook

__uri__ = 'mongodb://localhost:27017/'


def generateXSLX(registration, licenses, analitics):
	dir = os.getcwd()
	wb = Workbook(write_only=True)

	ws_lic = wb.create_sheet('Лицензии')
	ws_reg = wb.create_sheet('Регистрации', 0)
	ws_an = wb.create_sheet('Аналитика')

	ws_reg.append([
			'Дата регистрации',
			'Продукт',
			'Сумма оплат',
			'Имя',
			'Телефон',
			'Компания',
			'Email',
			'Отрасль',
			'Last Click CL (source)',
			'Last Click CL (channel)',
			'Last Click CL (campaign)',
			'Multi-Channel Funnels CL',
			'Last Click CB (source)',
			'Last Click CB (channel)',
			'Last Click CB (campaign)',
			'Multi-Channel Funnels CB'
	])
	for row in registration:
		ws_reg.append(list(row.values()))

	ws_lic.append([
		'Дата создания',
		'Email',
		'Тип лицензии',
		'Продукт',
		'Сумма оплаты',
		'Дата оплаты',
		'Название лицензии',
		'ID',
		'Слушатель вебинаров',
		'Каналы',
		'Начало доступа',
		'Конец доступа'
	])
	for row in licenses:
		ws_lic.append(list(row.values()))

	ws_an.append([
		'Дата оплаты',
		'Количество кликов зарегистрировавщихся',
		'Сумма расходов зарегистрировавщихся',
		'Количество кликов не зарегистрировавшихся',
		'Сумма расходов не зарегистрировавщихся',
		'Строка UTM',
		'Compaign UTM',
		'Medium UTM',
		'Source UTM',
		'Продукт'
	])
	for row in analitics:
		ws_an.append(list(row.values()))

	if not os.path.exists(dir + '/output'):
		os.makedirs(dir + '/output')

	wb.save(dir + "/output/report.xlsx")


def utmParse(utm, outputArray):
	result = {}

	if (not utm):
		return False

	for part in utm.split('&'):
		item = part.split('=')
		result[item[0]] = item[1]

	output = result.get('utm_source', '') + '/' + result.get('utm_medium', '') + ' ' + result.get('utm_campaign', '')

	if (outputArray):
		return result
	else:
		return output


def mapUtm(item):
	return utmParse(item['utm'], False)


def userRegistration(collection):
	dataset = []
	caselookId = '7683e781-527e-4636-b7e9-b1e7975a18f7'

	cursor = collection.aggregate([		
		{
			'$unwind': '$products'
		},
		{
			'$match': {
				'products.project.registration_date': {
					'$gte': dateutil.parser.parse('2017-09-01T00:00:00.000Z')
				}
			}
		},
		{
			'$project': {
				'email': 1,
				'products': 1,
				'pay_total': 1,
				'contacts': 1,
				'__v': 1
			}
		},
		{
			'$sort': SON([('products.project.registration_date', 1)])
		}
	])

	for user in cursor:
		products = user['products']
		project = products['project']
		company = project.get('company', '')

		try:
			utm = map(mapUtm, project["utm_sources"])
			list_sources = ','.join(utm)
		except AttributeError:
			print('Not utm')

		try:
			lastUtm = utmParse(project['last_mark']['utm'], True)
			last_utm_source = lastUtm['utm_source']
			last_utm_medium = lastUtm['utm_medium']
			last_utm_campaign = lastUtm['utm_campaign']
		except KeyError:
			last_utm_source = ''
			last_utm_medium = ''
			last_utm_campaign = ''

		if (products['_id'] == caselookId):
			productName = 'Caselook'
			last_clickCL_source = last_utm_source
			last_clickCL_channel = last_utm_medium
			last_clickCL_campaign = last_utm_campaign
			multi_channeelCL = list_sources
			last_clickCB_source = ''
			last_clickCB_channel = ''
			last_clickCB_campaign = ''
			multi_channeelCB = ''
		else:
			productName = 'Casebook'
			last_clickCB_source = last_utm_source
			last_clickCB_channel = last_utm_medium
			last_clickCB_campaign = last_utm_campaign
			multi_channeelCB = list_sources
			last_clickCL_source = ''
			last_clickCL_channel = ''
			last_clickCL_campaign = ''
			multi_channeelCL = ''

		try:
			if (project['sector']):
				sector = project['sector']['name']
		except KeyError:
			sector = ''

		try:
			dataset.append({
				'registration_date': project['registration_date'],
				'product': productName,
				'pay_total': project['pay_total'],
				'name': project['name'],
				'phone': project['phone'],
				'company': company,
				'email': user['email'],
				'sector': sector,
				'last_clickCL_source': last_clickCL_source,
				'last_clickCL_channel': last_clickCL_channel,
				'last_clickCL_campaign': last_clickCL_campaign,
				'multi_channeelCL': multi_channeelCL,
				'last_clickCB_source': last_clickCB_source,
				'last_clickCB_channel': last_clickCB_channel,
				'last_clickCB_campaign': last_clickCB_campaign,
				'multi_channeelCB': multi_channeelCB
			})
		except KeyError:
			print(user['product'])

	else:
		return dataset

def userLicenses(collection):
	dataset = []
	caselookId = '7683e781-527e-4636-b7e9-b1e7975a18f7'

	cursor = collection.find(
		{
			'create_date': {
				'$gte': dateutil.parser.parse('2017-09-01T00:00:00.000Z')
			}
		}
	).sort([
	  ('create_date', pymongo.ASCENDING)
	])

	for contract in cursor:
		channels = ','.join(contract.get('channels', '')).replace('"', '').replace('[', '').replace(']', '')
		productName = 'Caselook' if (contract['project'] == caselookId) else 'Casebook'
		is_webinar_user = 'Да' if (contract.get('is_webinar_user', '')) else 'Нет'

		try:
			dataset.append({
				'create_date': contract.get('create_date', ''),
				'email': contract.get('email', ''),
				'type': contract.get('license_type', ''),
				'product': productName,
				'pay_summ': contract.get('pay_summ', ''),
				'pay_date_time': contract.get('pay_date_time', ''),
				'name': contract.get('name', ''),
				'license_id': contract.get('license_id', ''),
				'is_webinar_user': is_webinar_user,
				'channels': channels,
				'starting_date': contract.get('starting_date', ''),
				'expiry_date': contract.get('expiry_date', '')
			})
		except KeyError:
			print(contract)

	else:
		return dataset

def userAnalitics(collection):
	dataset = []
	caselookId = '7683e781-527e-4636-b7e9-b1e7975a18f7'

	cursor = collection.find({
		'date': {
			'$gte': dateutil.parser.parse('2017-09-01T00:00:00.000Z')
		}
	}).sort([
	  ('date', pymongo.ASCENDING)
	])

	for cost in cursor:
		utm_sources = utmParse(cost['sourceString'], True)

		try:
			dataset.append({
				'date': cost.get('date', ''),
				'clicks_registered': cost.get('clicks_registered', ''),
				'total_registered': cost.get('total_registered', ''),
				'clicks_unregistered': cost.get('clicks_unregistered', ''),
				'total_unregistered': cost.get('total_unregistered', ''),
				'source_string': cost.get('sourceString', ''),
				'source_compaign': utm_sources.get('utm_campaign', ''),
				'source_medium': utm_sources.get('utm_medium', ''),
				'source_source': utm_sources.get('utm_source', ''),
				'product': cost.get('product', '')
			})
		except KeyError:
			print(cost)

	else:
		return dataset

def main():
	try:
		client = pymongo.MongoClient(__uri__)
		database = client.billing
		collection_reg = database['projectusers']
		collection_licenses = database['projectlicenses']
		collection_analitics = database['analitics']
		registration = userRegistration(collection_reg)
		licenses = userLicenses(collection_licenses)
		analitics = userAnalitics(collection_analitics)

		generateXSLX(registration, licenses, analitics)

	except:
		print('error')
		raise

	finally:
		print('done')
		client.close()

if __name__ == "__main__":
	main()
